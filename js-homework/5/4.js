// 4 Напишите функцию (), которая в зависимости от переданных 
// в нее целочисленных аргументов "количество дней", будет выводить 
// слово "день" в нужно форме ("если передали - 1, то 1 день", 
// "если передали - 3, то 3 дня" и т.д).   
function getDaysCount(num) {
    let normDays = num % 100;
    let lastNum = num % 10;
    if (lastNum === 0 || lastNum >= 5 || normDays > 10 && normDays < 20) {
        console.log(num + " дней");
    } else if (lastNum === 1) {
        console.log(num + " день");
    } else {
        console.log(num + " дня");
    }
}

// Тест
for (let idx = 0; idx < 10; idx++) {
    getDaysCount(Math.floor(Math.random() * 20) + 1);
    getDaysCount(Math.floor(Math.random() * 100) + 1);
}