// 2 Заданы два массива A и B необходимо их объединить 
// в один массив C так, чтобы в массиве остались уникальные 
// (неповторяющиеся) элементы. 
// Например: A = [1,2], B = [2,3] получим С = [1, 2, 3].
function addUniqueValues(target, origin) {
    for (let originIndex in origin) {
        let isNew = true;
        let targetIndex = 0;
        while (targetIndex < target.length) {
            if (origin[originIndex] === target[targetIndex]) {
                isNew = false;
                break;
            }
            targetIndex++;
        }
        if (isNew) {
            target.push(origin[originIndex]);
        }
    }
}

const A = [1, 2];
const B = [2, 3];
let C = [];

addUniqueValues(C, A);
addUniqueValues(C, B);

console.log(C);