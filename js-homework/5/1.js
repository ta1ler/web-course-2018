// 1 Построить объект студент со свойствами:
// Имя, Фамилия, Возраст, Интересы (в виде массива), Место обучения.
// Написать отдельную функцию, которой передается объект студент, 
// а она выводит его содержимое.
function show_student(student) {
	console.log(`${student.first_name} ${student.last_name} (${student.age}).\nStudent of ${student.place}.\nHobbies: ${student.hobbies.join(", ")}.`);
}

let student = {
	first_name : "Jon",
	last_name : "Snow",
	age : 18,
	hobbies : ["sword fighting", "dogs"],
	place : "the Wall"
};
show_student(student);