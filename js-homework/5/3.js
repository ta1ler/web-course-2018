// 3 Написать отдельную функцию, которая выводит пользователю 
// заданное число чисел Фибоначчи. 
// (например, первых 8 чисел Фибоначчи:  1, 1, 2, 3, 5, 8, 13, 21). 
// Заданное число передается функции в качестве аргумента. 
function getFibSeq(num) {
    if (num < 1) {
        return [];
    }

    if (num === 1) {
        return [1];
    }

    if (num === 2) {
        return [1, 1];
    }

    let result = [1, 1];
    for (let idx = 2; idx < num; idx++) {
        result.push(result[idx-2] + result[idx-1]);
    }
    return result;
}

console.log(getFibSeq(8));