// 1 Дана строка, изображающая целое число. 
// Вывести сумму цифр этого числа. 
const str = "123";
let arr = str.match(/\d/g);
let sum = 0;
for (idx in arr) {
	sum += parseInt(arr[idx]);
}
console.log(sum);