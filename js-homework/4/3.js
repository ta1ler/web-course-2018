// 3. Задан массив  - [12,4,3,10,1,20]. Удалить из него наименьшее и наибольшее значение.
let arr = [12, 4, 3, 10, 1, 20];
max = Math.max(...arr);
min = Math.min(...arr);
for (let idx in arr) {
	if (arr[idx] === max || arr[idx] === min) {
		delete arr[idx];
	}
}
console.log(arr);