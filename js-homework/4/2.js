// 2.
// Одномерным массивом задана доска 3 на 3 
// var area = [ null, null, null, null, null, null, null, null, null ]

// Необходимо сформировать игровое поле состоящее из квадратов для крестиков-ноликов  в HTML. 

// При появлении в массиве 0-ля рисовать нолик , 1-цы крестик 

// Пример:  [ 1, null, 0, null, 1, null, null, null, null ] на поле 2-а крестика, и 1-н нолик.
let area = [ 1, null, 0, null, 1, null, null, null, null ];
let str = "<table>";
for(let row = 0; row < 3; row++) {
	str += "<tr>";
	for(let col = 0; col < 3; col++) {
		switch(area[row * 3 + col]) {
			case(1):
				str += "<td>X</td>";
				break;
			case(0):
				str += "<td>0</td>";
				break;
			default:
				str += "<td> </td>";
		}
	}
	str += "</tr>";
}
str += "</table>";

document.write(str);