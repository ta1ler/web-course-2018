// 1. Заданы два массива 
// A = [ 12, 4, 3, 10, 1, 20 ]
// B = [-3, -7, -100, -33] 
// необходимо их объединить в один массив C добавив массив B в конец(в начало) A. 
const A = [ 12, 4, 3, 10, 1, 20 ];
const B = [-3, -7, -100, -33];
let C = A.concat(B);
console.log(C);
C = B.concat(A);
console.log(C);