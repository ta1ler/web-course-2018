// Bonus. 4. Задача повышенной сложности.
// Задан массив - [12,4,3,10,1,20] 
// необходимо отсортировать его в порядке возрастания, 
// при этом не использовать готовый метод sort и методы разобранные на занятии.

//Сортировка слиянием
function merge(left, right) {
	let result = [];

	while (left.length > 0 && right.length > 0) {
		if (left[0] <= right[0]) {
			result.push(left.shift());
		} else {
			result.push(right.shift());
		}
	}
	while (left.length > 0) {
		result.push(left.shift());
	}
	while (right.length > 0) {
		result.push(right.shift());
	}

	return(result);
}

function merge_sort(origin) {
	let left = [];
	let right = [];

	const len = origin.length;
	
	if (origin.length <= 1) {
		return(origin);
	} else {
		let middle = Math.floor(len / 2);
		for (let idx = 0; idx < middle; idx++) {
			left.push(origin[idx]);
		}
		for (let idx = middle; idx < len; idx++) {
			right.push(origin[idx]);
		}

		left = merge_sort(left);
		right = merge_sort(right);
		
		return(merge(left, right));
	}
}

let arr = [12, 4, 3, 10, 1, 20];
console.log(merge_sort(arr));