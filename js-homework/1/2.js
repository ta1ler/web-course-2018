function parse(num_str) {
    const num = parseInt(num_str);
    
    if (isNaN(num) || (num < -999) || (num > 999)) {
        return("Некорректный ввод");
    }
    
    if (num === 0) {
        return("нулевое число");
    }
    
    const sign_str = (num < 0) ? "отрицательное " : "положительное ";

    const abs_num = Math.abs(num);
    if (abs_num < 10) {
        return(sign_str + "однозначное число");
    } else if (abs_num < 100) {
        return(sign_str + "двухзначное число");
    } else {
        return(sign_str + "трехзначное число");
    }
}

alert( parse( prompt("Введите целое число от -999 до 999") ) );