// Единицы длины пронумерованы следующим образом: 
// 1 — дециметр, 2 — километр, 
// 3 — метр, 4 — миллиметр, 5 — сантиметр. 
// Дан номер единицы длины и длина 
// отрезка L в этих единицах (вещественное число). 
// Вывести длину данного отрезка в метрах.

const metr = 1;
const length = 31.1;
const accuracy = 0.001; //точность

let factor;
switch (metr) {
    case 1:
        factor = 0.1;
        break;
    case 2:
        factor = 1000;
        break;
    case 3:
        factor = 1;
        break;
    case 4:
        factor = 0.001;
        break;
    case 5:
        factor = 0.01;
        break;
    default:
        factor = 1;
}
alert( Math.round( (length * factor) / accuracy) * accuracy);