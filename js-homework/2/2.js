//Вершины, заданные целыми координатами x, y, z
let points = [[1, 2, 3], [2, 3, 1], [3, 1, 2]];
let vectors = [
	[points[0][0] - points[1][0], points[0][1] - points[1][1], points[0][2] - points[1][2]],
	[points[2][0] - points[1][0], points[2][1] - points[1][1], points[2][2] - points[1][2]],
	[points[0][0] - points[2][0], points[0][1] - points[2][1], points[0][2] - points[2][2]]
];
let scalars = [
	vectors[0][0] * vectors[1][0] + vectors[0][1] * vectors[1][1] + vectors[0][2] * vectors[1][2],
	vectors[2][0] * vectors[1][0] + vectors[2][1] * vectors[1][1] + vectors[2][2] * vectors[1][2],
	vectors[0][0] * vectors[2][0] + vectors[0][1] * vectors[2][1] + vectors[0][2] * vectors[2][2]
];

if (scalars[0] === 0 || scalars[1] === 0 || scalars[2] === 0) {
	alert("Прямоугольный");
} else {
	alert("Не прямоугольный");
}