let str = "<table><tr>";
for(let col = 0; col < 10; col++) {
	str += "<th>" + (col + 1) + "</th>";
}
str += "</tr>";
for(let row = 0; row < 10; row++) {
	str += "<tr>";
	for(let col = 0; col < 10; col++) {
		str += "<td>" + (row + 1) * (col + 1) + "</td>";
	}
	str += "</tr>";
}
str += "</table>";

document.write(str);