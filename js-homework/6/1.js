// 1 Построить объект студент:
// - свойства: Имя, Фамилия, Возраст, Интересы (в виде массива), Место обучения.
// - метод выводящий в консоль биографическую справку в виде, например: 
// «Иван Петров. 21 год. Интересы: программирование, музыка, аниме. Учится в ИТМО.
let student = {
    first_name : "Jon",
    last_name : "Snow",
    age : 18,
    hobbies : ["sword fighting", "dogs"],
    place : "the Wall",
    show: function() {
        console.log(`${this.first_name} ${this.last_name} (${this.age}).\nStudent of ${this.place}.\nHobbies: ${this.hobbies.join(", ")}.`);
    }
};

student.show();