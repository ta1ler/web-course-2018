// 3 Сделайте функцию, каждый вызов который будет генерировать случайное число 
// от 1 до 100, но так, чтобы оно не повторялось, пока не будут перебраны все 
// числа из этого промежутка. Решите задачу через замыкания - в замыкании должен 
// хранится массив чисел, которые уже были сгенерированы функцией.  
function Random() {
    function isHere(num) {
        for(let idx in produced) {
            if (produced[idx] === num) {
                return true;
            }
        }
        return false;
    }

    let produced = [];

    return function() {
        let result;
        do {
            result = Math.floor(Math.random() * 100) + 1;
        } while (isHere(result));
        produced.push(result);
        return result;
    }
}

getRandom = Random();
for (let count = 0; count < 10; count++) {
    console.log(getRandom());
}