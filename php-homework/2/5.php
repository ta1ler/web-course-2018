<?php
// 5. Дан массив $fruits. 
// Каждому вложенному массиву добавить count - количество элементов в массиве (элементы с одинаковым name)
// Удалить дублирующиеся элементы
$fruits = [
  [ "name"=> "apple",  "color"=> "red" ],
  [ "name"=> "orange", "color"=> "orange" ],
  [ "name"=> "lemon",  "color"=> "yellow" ],
  [ "name"=> "apple",  "color"=> "green" ],
  [ "name"=> "plum",   "color"=> "violet" ],
  [ "name"=> "plum",   "color"=> "violet" ],
  [ "name"=> "apple",  "color"=> "red" ],
  [ "name"=> "lemon",  "color"=> "yellow" ],
  [ "name"=> "banana", "color"=> "yellow" ]
];

$counts = array_count_values(array_column($fruits, 'name'));

foreach ($fruits as $key => &$fruit) {
  $fruit['count'] = $counts[$fruit['name']];
  unset($fruit['color']);
}
unset($fruit);

$fruits = array_map("unserialize", array_unique(array_map("serialize", $fruits)));

var_dump($fruits);
?>