<div style="font-family: Courier">
<?php
// 2. Нарисуйте треугольник (или ромб) из символов *. 
// Высота треугольника равна 15.
for ($i = 0; $i < 15; $i++) {
  for ($j = 0; $j < $i + 1; $j++) {
    if ($j == 0 || $i == $j || $i == 14) {
      echo "*";
    } else {
      echo "&nbsp;";
    }
  }
  echo "<br>";
}
?>
</div>