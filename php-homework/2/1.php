<table>
<?php
// 1. Вывести таблицу умножения чисел до 10 с помощью двух циклов for (вложенный цикл);
for ($i = 0; $i < 10; $i++) {
  echo "<tr>";
  for ($j = 0; $j < 10; $j++) {
    if ($i == 0 && $j == 0) {
      echo "<th></th>";
    } elseif ($i == 0) {
      echo "<th>$j</th>";
    } elseif ($j == 0) {
      echo "<th>$i</th>";
    } else {
      echo "<td>".($i * $j)."</td>";
    }
  }
  echo "</tr>";
}
?>
</table>