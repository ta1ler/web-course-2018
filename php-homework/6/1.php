<?php
// Создать фигуры: Circle, Rectangle, Triangle, у которых будут координаты. Все фигуры должны иметь методы, которые возвращают площадь и периметр (для окружности - длина окружности).

class Coordinates {
  public $x;
  public $y;

  public function __construct(float $coord_x, float $coord_y) {
    $this->x = $coord_x;
    $this->y = $coord_y;
  }
}

abstract class Figure {
  abstract function getPerimeter();
  abstract function getSquare();
}

class Circle extends Figure {
  private $coords;
  private $radius;

  public function __construct(float $radius, Coordinates $coords) {
    $this->radius = $radius;
    $this->coords = $coords;
  }

  public function getPerimeter() {
    return 2 * pi() * $this->radius;
  }

  public function getSquare() {
    return pi() * $this->radius ** 2;
  }
}

class Rectangle extends Figure {
  private $coords;
  private $width;
  private $height;

  public function __construct(Coordinates $coords_0, Coordinates $coords_1, Coordinates $coords_2, Coordinates $coords_3) {
    $this->coords = array($coords_0, $coords_1, $coords_2, $coords_3);
    $this->width = sqrt(($coords_0->x - $coords_1->x) ** 2 + ($coords_0->y - $coords_1->y) ** 2);
    $this->height = sqrt(($coords_0->x - $coords_2->x) ** 2 + ($coords_0->y - $coords_2->y) ** 2);
  }

  public function getPerimeter() {
    return 2 * ($this->width + $this->height);
  }

  public function getSquare() {
    return $this->width * $this->height;
  }
}

class Triangle extends Figure {
  private $coords;

  public function __construct(Coordinates $coords_0, Coordinates $coords_1, Coordinates $coords_2) {
    $this->coords = array($coords_0, $coords_1, $coords_2);
  }

  public function getPerimeter() {
    return sqrt(($coords_0->x - $coords_1->x) ** 2 + ($coords_0->y - $coords_1->y) ** 2) +
           sqrt(($coords_0->x - $coords_2->x) ** 2 + ($coords_0->y - $coords_2->y) ** 2) +
           sqrt(($coords_2->x - $coords_1->x) ** 2 + ($coords_2->y - $coords_1->y) ** 2);
  }

  public function getSquare() {
    return abs($coords_0->x * $coords_1->y + $coords_1->x * $coords_2->y + $coords_2->x * $coords_0->y -
               $coords_0->x * $coords_2->y - $coords_1->x * $coords_0->y - $coords_2->x * $coords_1->y) / 2;
  }
}