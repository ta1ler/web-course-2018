<?php
// Дом строится со следующими характеристиками: 
// Что из низ задается через конструктор, а что потом - решайте сами 
// 1. какое-то количество подъездов 
// 2. какое-то количество этажей 
// 3. какое-то количество квартир на этаже 
// 4. адрес 
// У дома должна быть возможность сообщить, сколько в нем квартир - метод 
// У дома должна быть возможность показать адрес - метод 
// У дома должна быть возможность сообщить, сколько людей в нем живет - метод 

// Есть очередь людей (массив с объектами класса Human), 
// каждый хочет заселиться в дом , но у каждого есть пожелание - этажность дома! 

// Одновременно в дом можно заселить троих. 
// house.add(); при вызове этого метода можно заселить только 3х людей 

// Людей из списка можно заселять в дом (по квартире на человека), 
// если в нем есть еще свободные квартиры 
// и если их пожелание можно учесть (желаемый этаж должен быть в доме). 

// Если человека нельзя поселить, потому что его пожелание не выполняется, 
// он перемещяется в конец очереди. 
// Если не хватило места - остается на прежнем месте, ждать нового дома

class House {
  private $entrancesNum;
  private $floorsNum;
  private $roomsNum;
  private $address;

  private $rooms = [];
  private $population = 0;

  public function __construct(int $entrancesNum, int $floorsNum, int $roomsNum, string $address) {
    $this->entrancesNum = $entrancesNum;
    $this->floorsNum = $floorsNum;
    $this->roomsNum = $roomsNum;
    $this->address = $address;

    for ($idx = 0; $idx < $entrancesNum; $idx++) { 
      $this->rooms[$idx] = array_fill(0, $floorsNum, $roomsNum);
    }
  }

  public function get_rooms_num() {
    return $this->roomsNum;
  }

  public function get_address() {
    return $this->address;
  }

  public function get_population() {
    return $this->population;
  }

  public function add(array &$queue) {
    for ($idx = 0; $idx < 3; $idx++) { 
      if (count($queue) < 1 || $this->is_full()) {
        return;
      }

      $human = array_shift($queue);

      if ($human->floorDesire > $this->floorsNum) {
        array_push($queue, $human);
        continue;
      }

      $added = false;
      $entranceIndex = 0;
      
      while (!$added || isset($this->rooms[$entranceIndex])) {
        if ($this->rooms[$entranceIndex][$human->floorDesire - 1] > 0) {
          $this->rooms[$entranceIndex][$human->floorDesire - 1]--;
          $added = true;
          $this->population++;
          break;
        }
        $entranceIndex++;
      }

      if (!$added) {
        array_push($queue, $human);
      }
    }
  }

  private function is_full() {
    return ($this->population == $this->entrancesNum * $this->floorsNum * $this->roomsNum);
  }
}

class Human {
  public $floorDesire;

  public function __construct(int $floorDesire) {
    $this->floorDesire = $floorDesire;
  }
}