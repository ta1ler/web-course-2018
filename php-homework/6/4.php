<?php
/*
Реализовать объектную модель: Яблоко, Дерево, Сад 

Программа должна уметь добавлять деревья в сад. 
Яблоки на деревья. 
Возвращать информацию о количестве деревьяв и яблок. 

Сад - это объект со списком объектов деревьев. 
Дерево - это объект со списком объектов яблок. 

Яблоки должны иметь определяться: 
возрастом 
цветом 
размером 
флаг испорченности (0 - свежее, 1 - испорченное) 
флаг упавшего с дерева (0 - на дереве, 1 - упало) 

Яблоки имею методы: 
упасть с дерева 
испортиться 

У всего сада есть возраст (например количество суток). 
Каждые 30 суток на каждом дереве рождается новое яблок. 
Все яблоки каждые сутки стареют на 1 день. 
Яблоки падают с дерева при возрасте 30 дней. 
Можно усложнить - 50% яблок могут упасть через 28 или через 32 дня по случайному выбору. 
Яблоки портятся, после падения через сутки 

Сад имеет метод: 
просчитать 1 сутки (т.е. метод, который фиксирует прохождение суток) 

Остальные методы и атрибуты объектов необходимо предусмотреть и реализовать. 

Пример результирующего кода: 

$garden = new Garder(); // создать сад, может быть создано N деревьев с N2 яблоками на каждом (N и N2 любые числа не больше 100, все яблоки при инцициализации создаются со случайным возрастом от 0 до 30) 

$garden->passDay(); // прошли одни сутки 
$garden->passDay(); // прошли одни сутки 
$garden->passDay(); // прошли одни сутки 
$garden->getCountApples(); // получить список висящих яблок на деревьях
$garden->passDay(); // прошли одни сутки 
$garden->getCountApples(); // получить список висящих яблок на деревьях этого сададеревьях
$garden->passDay(); // прошли одни сутки 
$garden->getCountApples(); // получить список висящих яблок на деревьях этого сада 

В задании могут быть добавлены иные условия и возможности, если будет интересно его усложнить (добавить время года, погоду и т.п. влияющие на рост/падение яблок, каждое упавшее яблоко может превращаться в новое дерево через время, следить за удалением яблок из массива, после того как они испортились и т.п.).
*/

class Apple {
  private $age;
  private $color;
  private $size;
  private $corrupted = false;
  private $fallen = false;

  static private $colors = ['red', 'green'];

  public function __construct() {
    $this->age = rand(1, 30);
    $this->color = $this->colors[rand(0, 1)];
    $this->size = rand(5, 15);
  }

  private function fall() {
    $this->fallen = true;
  }

  private function corrupt() {
    if ($this->fallen) {
      $this->corrupt = true;
    }
  }

  public function passDay() {
    $this->age++;

    if ($this->fallen) {
      if (!$this->corrupted) {
        $this->corrupt();
      }
      return;
    }

    if ($this->age >= 30) {
      $this->fall();
    }
  }
}

class Tree {
  private $apples = [];
  private $age;

  public function __construct() {
    $this->age = rand(1, 300);

    $n = rand(1, 100);
    for ($i = 0; $i < $n; $i++) { 
      $this->addApple(new Apple());
    }
  }

  public function addApple(Apple $apple) {
    array_push($this->apples, $apple);
  }

  public function getAppleCount() {
    return count($this->apples);
  }

  public function passDay() {
    $this->age++;

    if ($this->age % 30 == 0) {
      $this->addApple(new Apple());
    }

    foreach ($this->apples as $apple) {
      $apple->passDay();
    }
  }
}

class Garden {
  private $trees = [];

  function __construct() {
    $n = rand(1, 100);
    for ($i = 0; $i < $n; $i++) { 
      $this->addTree(new Tree());
    }
  }

  public function addTree(Tree $tree) {
    array_push($this->trees, $tree);
  }

  public function getTreeCount() {
    return count($this->trees);
  }

  public function getAppleCount() {
    $count = 0;
    foreach ($trees as $tree) {
      $count += $tree->getAppleCount();
    }
    return $count;
  }

  public function passDay() {
    foreach ($this->trees as $tree) {
      $tree->passDay();
    }
  }
}