<?php
// Сделать библиотеку, которая ведет учет книг. Должно быть как минимум два класса: Book и Library. Library имеет два метода: void put(Book book, int quantity) и int get(Book book, int quantity). Каждой книге в библиотеке соответствует счетчик, показывающий количество хранящихся книг, при добавлении книги - счетчик увеличивается, при извлечении - уменьшается на число quantity. 
// Поля класса Book: author, title, pagesNum. 
// Библиотека хранит ограниченное число книг, сколько - на ваше усмотрение. 

class Book {
  public $title;
  public $author;
  public $pagesNum;

  public function __construct(string $title, string $author, int $pagesNum) {
    $this->title = $title;
    $this->author = $author;
    $this->pagesNum = $pagesNum;
  }
}

class Library
{
  private $books = [];
  private $max;
  private $quantity = 0;

  public function __construct(int $max) {
    $this->max = $max;
  }

  public function put(Book $book, int $quantity) {
    if ($this->quantity + $quantity > $this->max) {
      $quantity = $this->max - $this->quantity;
    }

    $index = array_search($book, array_column($this->books, "book"));

    if ($index === false) {
      array_push($this->books, array("book" => $book, "quantity" => $quantity));
    } else {
      $this->books[$index] += $quantity;
    }

    $this->quantity += $quantity;
  }

  public function get(Book $book, int $quantity) {
    $index = array_search($book, array_column($this->books, "book"));

    if ($index === false) {
      return 0;
    }
    
    if ($this->books[$index]["quantity"] < $quantity) {
      $quantity = $this->books[$index]["quantity"];
    }

    $this->books[$index]["quantity"] -= $quantity;
    if ($this->books[$index]["quantity"] == 0) {
      unset($this->books[$index]);
    }

    return $quantity;
  }
}