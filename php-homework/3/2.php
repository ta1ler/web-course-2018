<?php
// Задача 2
// Допустим, пользователь вводит названия городов через пробел. Вы их присваиваете переменной.
// Переставьте названия так, чтобы названия были упорядочены по алфавиту.
$cities = "Moscow Saint-Petersburg Yekaterinburg Novosibirsk";
$cities = explode(" ", $cities);
sort($cities);
$cities = implode(" ", $cities);
echo $cities;
?>