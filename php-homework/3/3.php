<?php
// Задача 3
// Написать функцию - конвертер строки.
// Возможности:
// перевод всех символов в верхний регистр,
// перевод всех символов в нижний регистр,
// перевод всех символов в нижний регистр и первых символов слов в верхний регистр.
// Это должна быть одна функция
function convert_string(string $str, string $mode = 'UPPERCASE') {
  switch ($mode) {
    case 'UPPERCASE':
      return strtoupper($str);
    case 'LOWERCASE':
      return strtolower($str);
    case 'CAPITALIZE':
      return ucwords(strtolower($str));
    default:
      return "Invalid mode";
  }
}

echo convert_string("Let's chEck") . "<br>";
echo convert_string("Let's chEck", 'UPPERCASE') . "<br>";
echo convert_string("Let's chEck", 'LOWERCASE') . "<br>";
echo convert_string("Let's chEck", 'CAPITALIZE') . "<br>";
?>